use aws_sdk_dynamodb::{model::AttributeValue, Client};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

// data to push to the dynamodb table
#[derive(Debug, Serialize, Deserialize)]
pub struct RollDice {
    pub roll_id: String,
    pub roll_value: u8,
}

// required for AWS
#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

// implement Display for the Failure response so that we can then implement Error.
impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

// implement add 
#[derive(Deserialize)]
struct Request {
    command: String,
}

// the response will show what the person rolled or view last 10 results
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

// main body function to add or view results
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let r_id = event.context.request_id;
    // adding frolls first
    let mut rng = rand::thread_rng();
    let random_number: u8 = rng.gen_range(1..=6);
    let result = match random_number {
        1 => "1",
        2 => "2",
        3 => "3",
        4 => "4",
        5 => "5",
        6 => "6",
        _ => unreachable!(), // unreachable because the range is specified
    };
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);
    // plug in roll to request struct
    let mut dice_roll = RollDice {
        roll_id: String::new(),
        roll_value: 0, // Initialize to 0
    };
    // set value
    dice_roll.roll_id = String::from(r_id.clone());
    dice_roll.roll_value = random_number; // Assign the random number directly
    // prepare to store value
    let dice_roll_id = AttributeValue::S(dice_roll.roll_id.clone());
    let dice_roll_value = AttributeValue::N(dice_roll.roll_value.to_string());
    // add to dynamodb
    // store our data in the result table
    let _resp = client
        .put_item()
        .table_name("rolldice")
        .item("roll_id", dice_roll_id)
        .item("roll_value", dice_roll_value)
        .send()
        .await
        .map_err(|_err| FailureResponse {
            body: _err.to_string(),
        });

    // prepare the response
    let resp = Response {
        req_id: r_id,
        msg: format!("You rolled a {}.", result),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
