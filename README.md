# IDS 721 Week 5

[![pipeline status](https://gitlab.com/hxia5/ids_721_week_5/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids_721_week_5/-/commits/main)

## AWS API Endpoint
https://a82xxoijq2.execute-api.us-east-1.amazonaws.com/test1/ids_721_week_5

## Overview
* This is my repository ofIDS 721 Mini-Project 5 - Serverless Rust Microservice

## Purpose
- Create a Rust AWS Lambda function (or app runner)
- Implement a simple service
- Connect to a database


## Key Steps

1. Install Rust and Cargo-Lambda, instructions can be found [here](https://www.cargo-lambda.info/guide/installation.html) and [here](https://www.rust-lang.org/tools/install).

2. Create a new Rust project using the following command:
```bash
cargo new ids_721_week_5
```

3. Modify `main.rs` to create a simple service that rolls a dice.

3. Add necessary dependencies to the Cargo.toml file.

5. Run `cargo lambda watch` in one terminal, and `cargo lambda invoke --data-ascii "{ \"command\": \"roll\" }` in another terminal to test the function.

6. Sign up for an AWS account and create a new IAM user with programmatic access and attach the `lambdafull_access` and `iamfullaccess` policy.

7. Click on the user, under the `Security credentials` tab, click on `Create access key`.

8. Copy the `access key ID` and `secret access key`, select a region under the tab at the top right corner. Create a `.env` file in the root directory of the project and add the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`.

9. Put `.env` in `.gitignore` to avoid exposing sensitive information.

10. Build the project using `cargo lambda build` and deploy the project using `cargo lambda deploy`. However, there's something wrong with my laptop, since my code can run, I use `Makefile` to deploy the project.

11. Create a `.yml` file for the pipeline and enable auto build, test, and deploy of your lambda function every time you push.

12. After it paased, search for `Lambda` in the AWS Management Console, after redirecting to the Lambda service, make sure the region is the same as the one in the `.env` file.

13. Your function should be deployed and ready to be tested. Click on the function and go to the `Configuration` tab, and click on `Permissions` then click the `Role name` to add the `AmazonDynamoDBFullAccess` policy.

14. Then click on `Add trigger` to create API endpoint. Select `API Gateway`, `Create a new API`, and `REST API`. Set the Security as `Open`, click on `Additional settings` and set the stage name as you want and then click on `Add`.

15. Click on the API you just created, in `Resource`, under `Integration request`, make sure `Lambda proxy integration` is set as `False` and select the recommended settings for `Input passthrough`.

16. Click on `Deploy API` and select the stage you just created, and click on `Deploy`.

17. After getting the endpoint, use the command 
```
curl -X POST <your endpoint> \
  -H 'content-type: application/json' \
  -d '{ "command": "roll" }'
```
to test the function. 

## Successful Test Result

![alt text](<API test.png>)

## DynamoDB Table

![alt text](DB.png)
